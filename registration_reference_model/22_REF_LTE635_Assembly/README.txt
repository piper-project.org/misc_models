PIPER geometrical reference model based on a manual segmentation. 
See the documentation for more info.
Author: CEESAR, SOTON, UCBL-Ifsttar
Version: 1.0.0
License: Creative Commons Attribution 4.0 International License.
(https://creativecommons.org/licenses/by/4.0/)

This work has received funding from the European Union Seventh Framework
Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
Contributors include Pierre Maillez, Christophe Lecomte (SOTON), Erwan Jolivet
(CEESAR), Philippe Beillas (UCBL-Ifsttar)

This work is derived from a CT scan provided by CEESAR under a CCBY-4.0 License.

Remarks
* Some of the landmarks are incorrectly defined in the pmr file
* there is a scanning artefact on the head skin

Please check www.piper-project.org for more info and downloads 
(including the original CT scan data used to gererate this model)

