Copyright (C) 2017 TU Berlin
	
This file is part of the PIPER Environment Models (PIPER ENV).

PIPER ENV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PIPER ENV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
	
You should have received a copy of the GNU General Public License
along with the PIPER ENV. If not, see <http://www.gnu.org/licenses/>.
	
According to the term of the GPL v3 article 7, the Licenser hereby agrees to
include to GPL v3 the following additional terms:
- in no event, unless required by applicable law, shall any copyright holder,
or any other third party who modifies and/or conveys the model as permitted 
in accordance with this legal license, be liable to any other Party or third 
party for any indirect or direct damages, including but not limited to death,
bodily injury and property damages (public or private), even if such holder 
or other party has been advised of the possibility of such damages. Any use 
of the model is at your own risk.
	
This work has received funding from the European Union Seventh Framework 
Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
	
Contributors include:
Stefan Kirscht, Ahmed Saeed, William Goede, Martin Sitte [TU Berlin]

--------------------------------------------------------------------------------

PIPER ENV v2.0 RELEASE NOTES
Apr 2017

(1) INTRODUCTION

The PIPER Vehicle Environment Model (VEM) version 2.0 was released as a further
development of PIPER VEM V1.0. The VEM represents a generic and simplified
vehicle interior which contains the following assembly groups: Front Seats (FS),
Rear Seats (RS), Instrument Panel (IP) and Side Structures (SS). The model�s
measurements are based on VW Golf, Opel Astra and Ford Focus. The VEM can be
used for accident simulations and can be adapted to different vehicles by
parameter changes. The model was built for the LS-Dyna solver code.


(2) MODEL ORGANISATION AND USAGE

The complete model consists of 4 assembly groups for the left and right side of
the vehicle model, respectively. The model�s main file (.dyn) contains all
relevant control commands, includes and parameters to transform the assembly groups at
the respective side of the simplified interior.

For modifications and positioning, the assembly groups should be altered at a
local level first by modifying the respective parameters in the assembly group
files. These files also contain definitions of the boundary conditions, includes
and parameters to transform the components of the assembly groups. To translate,
rotate or scale the assembly groups the parameters or in the model master file
can be modified. A specific syntax was applied to the parameters as commented in
the master file and in the sub-files.

The enlosed file PIPER_VEM_v2.0_IncludeStructure.png illustrates the principle
how the vehicle model should be assembled. Furthermore, the filenames of the
sub-files of some main components and its parts are shown.

The following table shows how the naming convention operates on component names
in the PIPER VEM V2.0 model.

Assembly Name       |Aspect          |Component Name |Element Type |Part Nature
--------------------|----------------|---------------|-------------|-----------
FS Front Seat       |L Left          |SeatCushion    |1D           |Rigid
RS Rear Seat        |R Right         |SeatBackrest   |2D           |Null
IP Instrument Panel |MR Middle Right |Gas Pedal      |3D           |Blank
SS Side Structure   |ML Middle Left  |...            |             |
                    |Blank           |               |             |

(e. g. FS_L_SeatCushion_3D: seat cushion of the left front seat)

For a reasonable workflow, the parameters should be set at first. Then the
subsystem structure should be merged into a single file to allow a better
usability, e.g. when copying the new node positions of the seats after initial
simulations. This can be achieved with most pre-processors, e.g. in LS PrePost
by removing the box check in the �BySubSystem� option in the �Save Keyword�
window. Afterwards, all *DEFINE_TRANSFORM and *PARAMETER (as well as comments)
can be deleted to keep the model clear.

The original overall numbering range (nodes and elements) of the PIPER VEM V2.0
lies between 1.000.000 � 1.999.999. An additional ID offset was implemented by
an additional parameter in the main model file to prevent an overlap with the
PIPER Child HBM. The following table shows the stated numbering range of the
assemblies of the model and the amount of parts in these assemblies.

Assembly (Side) | Element/Node Range    | Parts Start ID (Amount)
----------------|-----------------------|------------------------
Structure (L)   | 1.000.000 - 1.049.999 | 1.000 (20)
Structure (R)   | 1.050.000 - 1.099.999 | 1.050 (14)
Front Seat (L)  | 1.100.000 - 1.299.999 | 1.100 (12)
Front Seat (R)  | 1.300.000 - 1.499.999 | 1.300 (12)
Rear Seat (L)   | 1.500.000 - 1.749.999 | 1.500 (18)
Rear Seat (R)   | 1.750.000 - 1.999.999 | 1.750 (18)

The next tables show the numbering range of the nodes and elements of the
structure assembly groups and the amount of parts in these groups for the left
and right side, respectively.

                         Structure (L)
---------------------|-----------------------|----------------------
Parts (Amount)       | Node IDs              | Element IDs
---------------------|-----------------------|----------------------
Floor Left (3)       | 1.000.000 - 1.004.039 | 1.000.000 - 1.007.537
Gas Pedal (1)        | 1.004.040 - 1.004.151 | 1.007.538 - 1.007.749
Instrument Panel (5) | 1.004.152 - 1.006.731 | 1.007.750 - 1.012.593
Steering Wheel (5)   | 1.006.732 - 1.014.057 | 1.012.594 - 1.023.239
Side Structure (9)   | 1.014.058 - 1.023.671 | 1.023.240 - 1.040.826

                         Structure (R)
---------------------|-----------------------|----------------------
Parts (Amount)       | Node IDs              | Element IDs
---------------------|-----------------------|----------------------
Floor Left (3)       | 1.050.000 - 1.054.147 | 1.050.000 - 1.057.742
Instrument Panel (5) | 1.054.148 - 1.056.829 | 1.057.743 - 1.062.783
Side Structure (9)   | 1.056.830 - 1.066.443 | 1.062.784 - 1.080.370


(3) MODEL VALIDATION

For the material definitions of the model, no validations were performed so far.