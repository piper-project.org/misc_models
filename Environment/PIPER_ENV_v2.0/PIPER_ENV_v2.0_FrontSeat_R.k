$ Copyright (C) 2017 TU Berlin
$
$ This file is part of the PIPER Environment Models (PIPER ENV).
$
$ PIPER ENV is free software: you can redistribute it and/or modify
$ it under the terms of the GNU General Public License as published by
$ the Free Software Foundation, either version 3 of the License, or
$ (at your option) any later version.
$
$ PIPER ENV is distributed in the hope that it will be useful,
$ but WITHOUT ANY WARRANTY; without even the implied warranty of
$ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
$ GNU General Public License for more details.
$
$ You should have received a copy of the GNU General Public License
$ along with the PIPER ENV. If not, see <http://www.gnu.org/licenses/>.
$
$ According to the term of the GPL v3 article 7, the Licenser hereby agrees to
$ include to GPL v3 the following additional terms:
$ - in no event, unless required by applicable law, shall any copyright holder,
$ or any other third party who modifies and/or conveys the model as permitted
$ in accordance with this legal license, be liable to any other Party or third
$ party for any indirect or direct damages, including but not limited to death,
$ bodily injury and property damages (public or private), even if such holder
$ or other party has been advised of the possibility of such damages. Any use
$ of the model is at your own risk.
$
$ This work has received funding from the European Union Seventh Framework
$ Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
$
$ Contributors include:
$ Stefan Kirscht, Ahmed Saeed, William Goede, Martin Sitte [TU Berlin]
$
*KEYWORD
$
$    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$    #                                                                         #
$    #    PIPER Project (Grant agreeement no. 605544)                          #
$    #                                                                         #
$    #    Technische Universitšt Berlin                                        #
$    #    Fachgebiet Kraftfahrzeuge (KFZB)                                     #
$    #                                                                         #
$    #    PIPER Vehicle Environment Model                                      #
$    #    Model Version: 2.0                                                   #
$    #    Assembly: Front Seat Right                                           #
$    #    Units:         S3 (mm - ms - kg - kN)                                #
$    #                                                                         #
$    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$
*PARAMETER
$ The local assembly parameters of the VEM are organised with the following syntax:
$ * Assembly        2 letters:     fl=floor; fs=front seat; rs=rear seat; ip=instrument panel; rf=roof; ss=side structure
$ * Component       2 letter:      gp=gas pedal; fe=front extension; ml= main lower floor; mt=middle tunnle; se=side extension;
$                                  a1= belt attachment (number 1-2 for ISOFIX attachments); bl= belt lock; hr= head rest; sb= seat back; sc=seat cushion;
$                                  da=Dashboard; sw= steering wheel;
$                                  ws=windscreen; ro=roof;
$      											   		 fw= front window; rw=rear window; pi= pillars; fd= front door; rd= rear door; fs= front window sill; rs= rear window sill
$ * Location        1 letter:      l= left; r=right; ml=middle left; mr=middle right
$ * Parameter Type  1 letter:      s=scale; t=translation; r=rotation
$ * Direction       1 letter:      x; y; z
$
$
$
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$ # # # # #     PARAMETERS      # # # # # # # # # # # # # # # # # # # # # # # #
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$
$_______________SEAT BACKREST___________________________________________________
$
$------------------------------------Scaling------------------------------------
R fssbrsx 1         R fssbrsy 1         R fssbrsz 1
$----------------------------------Translation----------------------------------
R fssbrtx 0         R fssbrty 0         R fssbrtz 0
$-----------------------------------Rotation------------------------------------
R fssbrry 0
$
$
$_______________SEAT CUSHION____________________________________________________
$
$------------------------------------Scaling------------------------------------
R fsscrsx 1         R fsscrsy 1         R fsscrsz 1
$----------------------------------Translation----------------------------------
R fsscrtx 0         R fsscrty 0         R fsscrtz 0
$-----------------------------------Rotation------------------------------------
R fsscrry 0
$
$
$_______________SEAT HEADREST___________________________________________________
$
$------------------------------------Scaling------------------------------------
R fshrrsx 1         R fshrrsy 1         R fshrrsz 1
$----------------------------------Translation----------------------------------
R fshrrtx 0         R fshrrty 0         R fshrrtz 0
$
$
$_______________BELT LOCK_______________________________________________________
$
$------------------------------------Scaling------------------------------------
R fsblrsx 1         R fsblrsy 1         R fsblrsz 1
$----------------------------------Translation----------------------------------
R fsblrtx 0         R fsblrty 0         R fsblrtz 0
$-----------------------------------Rotation------------------------------------
R fsblrry 0         R fsblrrz 0
$
$
$_______________BELT ATTACHMENT 01______________________________________________
$
$------------------------------------Scaling------------------------------------
R fsa1rsx 1         R fsa1rsy 1         R fsa1rsz 1
$----------------------------------Translation----------------------------------
R fsa1rtx 0         R fsa1rty 0         R fsa1rtz 0
$
$
$_______________BELT ATTACHMENT 02______________________________________________
$
$------------------------------------Scaling------------------------------------
R fsa2rsx 1         R fsa2rsy 1         R fsa2rsz 1
$----------------------------------Translation----------------------------------
R fsa2rtx 0         R fsa2rty 0         R fsa2rtz 0
$
$
$
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$ # # # # #     COMPONENT TRANSFORMATIONS     # # # # # # # # # # # # # # # # #
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$
*DEFINE_TRANSFORMATION
$_______________SEAT BACKREST___________________________________________________
$#  tranid
      1300
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fssbrsx  &fssbrsy  &fssbrsz         0.0       0.0       0.0       0.0
POINT          13000   993.731  -587.965   177.909
POINT          13001   993.731  -120.965   177.909
ROTATE         13000     13001&fssbrry
TRANSL    &fssbrtx  &fssbrty  &fssbrtz         0.0       0.0       0.0       0.0
$
*DEFINE_TRANSFORMATION
$_______________SEAT CUSHION____________________________________________________
$#  tranid
      1301
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fsscrsx  &fsscrsy  &fsscrsz         0.0       0.0       0.0       0.0
POINT          13010  1017.707  -571.272   120.247
POINT          13011  1017.707  -120.272   120.247
ROTATE         13010     13011&fsscrry
TRANSL    &fsscrtx  &fsscrty  &fsscrtz         0.0       0.0       0.0       0.0
$
*DEFINE_TRANSFORMATION
$_______________SEAT HEADREST___________________________________________________
$#  tranid
      1302
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fshrrsx  &fshrrsy  &fshrrsz         0.0       0.0       0.0       0.0
POINT          13000   993.731  -587.965   177.909
POINT          13001   993.731  -120.965   177.909
ROTATE         13000     13001&fssbrry
TRANSL    &fshrrtx  &fshrrty  &fshrrtz         0.0       0.0       0.0       0.0
$
*DEFINE_TRANSFORMATION
$_______________BELT LOCK_______________________________________________________
$#  tranid
      1303
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fsblrsx  &fsblrsy  &fsblrsz         0.0       0.0       0.0       0.0
POINT          13030  1024.421  -190.838   178.923
POINT          13031  1024.421  -185.838   178.923
ROTATE         13030     13031&fsblrry
POINT          13032  1024.421  -190.838   178.923
POINT          13033  1024.421  -190.838   185.923
ROTATE         13032     13033&fsblrrz
TRANSL    &fsblrtx  &fsblrty  &fsblrtz         0.0       0.0       0.0       0.0
$
*DEFINE_TRANSFORMATION
$_______________BELT ATTACHMENT 01______________________________________________
$#  tranid
      1304
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fsa1rsx  &fsa1rsy  &fsa1rsz         0.0       0.0       0.0       0.0
TRANSL    &fsa1rtx  &fsa1rty  &fsa1rtz         0.0       0.0       0.0       0.0
$
*DEFINE_TRANSFORMATION
$_______________BELT ATTACHMENT 02______________________________________________
$#  tranid
      1305
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fsa2rsx  &fsa2rsy  &fsa2rsz         0.0       0.0       0.0       0.0
TRANSL    &fsa2rtx  &fsa2rty  &fsa2rtz         0.0       0.0       0.0       0.0
$
$
$
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$ # # # # #     COMPONENT INCLUDES      # # # # # # # # # # # # # # # # # # # #
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$
*INCLUDE
PIPER_ENV_v2.0_FL_R_MainFloor.k
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_R_SeatBackrest.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1300
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_R_SeatCushion.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1301
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_R_SeatHeadrest.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1302
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_R_BeltLock.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1303
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_R_BeltAttachment1.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1304
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_R_BeltAttachment2.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1305
$
$
$
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$ # # # # #     ASSEMBLY CONSTRAINTS    # # # # # # # # # # # # # # # # # # # #
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$
*CONSTRAINED_RIGID_BODIES
$ Floor Assembly
      1051      1050         0
      1051      1052         0
$ Belt Buckle Assembly
      1311      1308         0
*BOUNDARY_SPC_SET_ID
$#      id                                                               heading
      1306Constraint_FS_R_BeltAttachment1
$#    nsid       cid      dofx      dofy      dofz     dofrx     dofry     dofrz
      1306         0         1         1         1         1         1         1
$
$#      id                                                               heading
      1307Constraint_FS_R_BeltAttachment2
$#    nsid       cid      dofx      dofy      dofz     dofrx     dofry     dofrz
      1307         0         1         1         1         1         1         1
$
$#      id                                                               heading
      1309Constraint_FS_R_BeltLashJoint
$#    nsid       cid      dofx      dofy      dofz     dofrx     dofry     dofrz
      1309         0         1         1         1         1         1         1
*CONSTRAINED_EXTRA_NODES_SET
$#     pid      nsid     iflag
      1308      1310         0
*END
